#define NUMREADINGS 7

int pinoDrive[6] = {9, 10, 8, 2, 3, 4}; // stepper pin//direction pin//Reset Pin//Mode 0 pin//Mode 1 pin// Mode 2 pin//

int pinoLDRS [4] = {0,1,2,3}; //LdrE Pin// LdrD Pin// LdrE1 Pin// LdrD1Pin//

const int nstepsMin = 10; 
const int nstepsMax = 25;
const float numbersStepsMin = 1;
const float numbersStepsMax = 3453;
const float mintolerance = 2;
const float maxtolerance = 60;
const int stepDelay = 1000;   //time between steps//

boolean dir;

int readingsH[NUMREADINGS][2];       
int readingsV[NUMREADINGS][2];
int indice = 0; //  current index 
int leituraE;
int leituraD;
float leituraE1;
float leituraD1;  // variable to store sensor readings//
float totalD =0;
float totalE=0;
float totalD1=0;
float totalE1= 0; // total moving average//
float error; 
int newCustom ;


float ldrEvalue =0;
float ldrDvalue = 0;
float ldrE1value = 0;
float ldrD1value= 0; 
float difldr =0;
float difldr1= 0;   // difference between the two LDR's in the same axis//
const int K = 1.28; 
const int K1 = 1.44;
const float angulominimo = -169.73;
const float angulomaximo = 169.73;

int contadorpinodrive; // used to store the pins attached to Driver
int contadorpinoLDR; //used to store the pins attached to the LDR sensor
int nP = 6; // number of pins attached to the DRV8825
int nLDR=4;  // number of LDR's (2 for each axis)

void setup() {

for (contadorpinodrive=0;contadorpinodrive<nP;contadorpinodrive++) {
pinMode(pinoDrive[contadorpinodrive], OUTPUT); // Drive attached to the digital pins //
}

for (contadorpinoLDR=0; contadorpinoLDR<nLDR; contadorpinoLDR++){
pinMode(pinoLDRS[contadorpinoLDR], INPUT);  // Sensor attached to the analogic pins//
}

digitalWrite(0,HIGH);  /// DRV8825 step modes (1/8 mode)
digitalWrite(1, HIGH);
digitalWrite(2, LOW);
Serial.begin(9600);

for (int i = 0; i < NUMREADINGS; i++){ //numbers of readings in moving average//
readingsH[NUMREADINGS][2]=0;
readingsV[NUMREADINGS][2]=0;  // start all the reading with 0//
}
}
 
void loop()
{
 int time1= millis();
rastreamento();
delay(250);
rastreamento1();
//posicionamentopotenciometro();
 //delay(1000);
 //correcao();
 int time2 =millis();
 float diftime = time2 - time1;
//m  Serial.println(diftime); 
 }
  void resetar (){
}
 void defineeixo (){
  int eixoH[2]  = {0,1};
  int eixoV[2] = {2,3};
 }
  
void mediamovel (char eixo) {   //do 7 readings to obtain a average value for the LDR's readings //
if (eixo= H) {
  float colunaA[2]={0,1};
  total-= readings[indice][colunaA[2]];
  leitura=analogRead(colunaA[2]];
  readings[indice][coluna[2]]=sqrt(10*analogRead(colunaA[2]));
  total+=readings[indice][colunaA[2]];
  indice = (indice + 1);
  else {
    /if (eixo=V){
  float colunaB[2]={2,3};
  total-= readings[indice][colunaA[2]];
  leitura=analogRead(colunaB[2]];
  readings[indice][colunaB[2]]=sqrt(10*analogRead(colunaB[2]));
  total+=readings[indice][colunaB[2]];
  indice = (indice + 1);}
    }
  }
if (indice >= NUMREADINGS)   {            // if the index is in the end of the vector//
indice = 0;                            // ... it comes back to the begin of the index (or 0)
ldrvalue= (total)/ NUMREADINGS;  // // calculate the moving average // } 
}

void int rastreamento (){ // this function is responsible for tracking the sun, by making comparison between the two LDR's in one axis//

for(int i=1;i<=7;i++)
{
mediamovel(); //  call for the function seven times to fill in the matrix of moving average and generate the average value  from the readings // 
}
float medValue = ((ldrEvalue + ldrDvalue)/2) ; // calculate the average value between the LDRs located in opposite sides//
difldr = (ldrEvalue - ldrDvalue); // calculate the difference between the average value from each side (left or right) //
float correction = 100*(difldr/medValue);
error = abs(correction); //value for making comparison to minimum thresold//
while(error > mintolerance)
{
mediamovel(); // call for the function moving average //
float medValue = ((ldrEvalue + ldrDvalue)/2) ;
difldr = (ldrEvalue - ldrDvalue);
float correction = 100*(difldr/medValue);
error = abs(correction);
digitalWrite(8,HIGH); // turn on the RESET Pin//
if (correction<0)
{
dir = HIGH; // if the signal of the correction is negative, the motor wil rotate to one direction//
}
else
{
dir = LOW;  //if the signal of the correction is positive, the motor wil rotate to other direction//
}
newCustom = map(error,mintolerance,maxtolerance,nstepsMin,nstepsMax); // function to generate the numbers of steps acording to the error (thresold)//
digitalWrite(10,dir); //set the Direction Pin//
for(int i =1; i<=newCustom;i++){    //number of iterations acording to the numbers of steps//
digitalWrite(9, HIGH); // turn on the Step Pin//
delayMicroseconds(stepDelay);
digitalWrite(9,LOW);  // turn off the Step Pin//
delayMicroseconds(stepDelay);  
}
}  
//Serial.println(String(dir) + ";"+ (newCustom) +";"+ldrEvalue +";"+ldrDvalue+";"+error+";"+leituraE+";"+leituraD+";" +readingsE[indice]+";"+readingsD[indice]);
//Serial.println(String(dir) + ";"+ (newCustom) +";"+ldrEvalue +";"+ldrDvalue+";"+error+";"+leituraE+";"+leituraD+";" +readingsE[indice]+";"+readingsD[indice]);
digitalWrite(8,LOW);
}  


void rastreamento1 (){ // it works at the same way as the previous function (rastreamento)//

  for(int i=1;i<=7;i++)
  {
  mediamovel1();
  }
  float medValue = ((ldrE1value + ldrD1value)/2) ;
  difldr1 = (ldrE1value - ldrD1value);
  float correction = 100*(difldr1/medValue);
  error = abs(correction);
  while(error > mintolerance)
  {
  mediamovel1();
  float medValue = ((ldrE1value + ldrD1value)/2) ;
  difldr1 = (ldrE1value - ldrD1value);
  float correction = 100*(difldr1/medValue);
  error = abs(correction);
  digitalWrite(8,HIGH);
  if (correction<0)
  {
   dir = HIGH;
  }
  else
  {
    dir = LOW;
  }
  newCustom = map(error,mintolerance,maxtolerance,nstepsMin,nstepsMax);
  digitalWrite(10,dir);
 for(int i =1; i<=newCustom;i++){
  digitalWrite(9, HIGH);
  delayMicroseconds(stepDelay);
  digitalWrite(9,LOW);
  delayMicroseconds(stepDelay);
  }
  //Serial.println(String(dir) + ";"+ (newCustom) +";"+ldrE1value +";"+ldrD1value+";"+error+";"+leituraE1+";"+leituraD1+";" +readingsE1[indice]+";"+readingsD1[indice]);
}
//Serial.println(String(dir) + ";"+ (newCustom) +";"+ldrE1value +";"+ldrD1value+";"+error+";"+leituraE1+";"+leituraD1+";" +readingsE1[indice]+";"+readingsD1[indice]);
digitalWrite(8,LOW);
}  

void posicionamentopotenciometro(){ //this aims to assure the right positioning of the solar panel, acording to the potentiometer readings// 

float potValue = analogRead(4);  // read the potentiometer//
float angle = map(potValue,0,1023,angulominimo,angulomaximo); //function to determine the angle value to be going through, acording to the potentiometer reading// 
float nSteps;
Serial.println(angle);

if (angle < 0) //if the angle is negative//
{
digitalWrite(8,HIGH); //turn on the RESET Pin//
dir=HIGH;  // set the movement to one direction
digitalWrite(10,dir); // set the Direction Pin//
nSteps = map(angle,angulominimo,0,755,0); // numbers of steps needed to reach the desired position acording to the angle determined earlier//
numerodepassos(nSteps);
}
else            // if the angle is positive//
{
 digitalWrite(8,HIGH); //turn on the RESET Pin//
dir = LOW;   // set the movement to the opposite direction//
 digitalWrite(10,dir); //set the Direction Pin//
nSteps= map (angle,1,angulomaximo,0,755); // numbers of steps needed to reach the desired position acording to the angle determined earlier//
numerodepassos(nSteps);
}
digitalWrite (8, LOW); // turn off the RESET Pin//
}  

 void posicionamentopotenciometro1(){ //// it works at the same way as the previous function (posicionamentopotenciometro)//

  float potValue = analogRead(5); 
  float angle = map(potValue,0,1023,angulominimo,angulomaximo);
  float nSteps;
  Serial.println(angle);
  
  if (angle < 0)
  {
   digitalWrite(8,HIGH);
   dir=HIGH; 
   digitalWrite(10,dir); 
   nSteps = map(angle,angulominimo,0,755,0);
   numerodepassos(nSteps);
  }
  else 
  {
      digitalWrite(8,HIGH);
    dir = LOW;
      digitalWrite(10,dir);
    nSteps= map (angle,1,angulomaximo,0,755);
    numerodepassos(nSteps);
  }
  digitalWrite (8, LOW);
 }  

  void  numerodepassos(int nSteps){ // function to generate pulses acording to the information obtained from the function (posicionamentopotenciometro)//

  for(int i =1; i<=nSteps;i++){
  digitalWrite(9, HIGH);  // turn on the Step Pin//
  delayMicroseconds(stepDelay);
  digitalWrite(9,LOW);  // turn off the Step Pin//
  delayMicroseconds(stepDelay);

  }
  }

  void correcao () {
    
    float leitura1= analogRead(A2);
    //float anglecorrecao = map(leitura1,0,1023,angulominimo,angulomaximo);

    if (leitura1<512){

   digitalWrite(nRESET,HIGH);
   dir=HIGH; 
   digitalWrite(dirPin,dir);
   numerodepassos(200); 
     
    }
   
    else {
      
   digitalWrite(nRESET,HIGH);
   dir=LOW; 
   digitalWrite(dirPin,dir);
   numerodepassos(200); 
     
    }
    float leitura2= analogRead(A2);
    float fatordecorrecao= 512*((200*0.225*0.221)/(leitura1 - leitura2));
    Serial.println(String(leitura1) + ";"+ (leitura2) + ";" + (fatordecorrecao));
    digitalWrite (nRESET, LOW);   
  }


    
